import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Quiz {
    private ArrayList<Question> questions;
    private ArrayList<Question> availableQuestionsForUser;
    Scanner scanner = new Scanner(System.in);
    double totalScore;
    private ArrayList<Question> wrongQuestions;
    long startTime;
    long endTime;

    public static void main(String[] args) {

        Quiz quiz = new Quiz();
        quiz.startQuiz();

    }

    private void addQuestions() {
        System.out.println("Время ввести вопросы! Когда захотите закончить, введите 'end'. У вас есть возможность сделать несколько правильных ответов, либо же сделать мульти-выбор");
        System.out.println("Чтобы сделать мульти-выбор введите 'мульти'");
        boolean isMulti;
        int countOfRightAnswers;
        while (true) {
            isMulti = false;
            System.out.println("Введите текст вопроса");
            String question = scanner.nextLine();
            if (question.equals("end")) {
                break;
            }
            if (question.equals("мульти")) {
                question = "Какие из данных утверждений верны";
                isMulti = true;
            }
            System.out.println("Введите кол-во ответов");
            int answersCount = Integer.parseInt(scanner.nextLine());
            ArrayList<Answer> answers = new ArrayList<>();
            countOfRightAnswers = getAnwersFromUser_returnCountOfRightAnswers(answers, answersCount);
            if (countOfRightAnswers > 1) {
                isMulti = true;
            }
            questions.add(new Question(question, answers, isMulti, countOfRightAnswers));
        }
    }

    private int getAnwersFromUser_returnCountOfRightAnswers(ArrayList<Answer> answers, int answersCount) {
        System.out.println("Сколько Вы хотите поставить баллов за (каждый) правильный ответ?");
        double answerPrice = Double.parseDouble(scanner.nextLine());
        System.out.println("Поочередно вводите ответы. Пометьте правильный ответ символом '+'");
        int countOfRightAnswers = 0;
        for (int j = 0; j < answersCount; j++) {
            StringBuilder strAnswer = new StringBuilder(scanner.nextLine());
            boolean isRight = strAnswer.charAt(strAnswer.length() - 1) == '+';
            if (isRight) {
                strAnswer.deleteCharAt(strAnswer.length() - 1);
                countOfRightAnswers++;
            } else {
                answerPrice = 0;
            }
            answers.add(new Answer(strAnswer.toString(), isRight ? Status.RIGHT : Status.WRONG, answerPrice));
        }
        return countOfRightAnswers;
    }

    private void startQuiz() {
        System.out.println("Добро пожаловать в наше приложение по созданию тестов. Давайте начнем!");
        while (true) {
            creatorStep();
            System.out.println("Готовы начать? Введите 'да', когда будете готовы");
            scanner.nextLine();
            startTime = System.currentTimeMillis();
            userStep();
            System.out.println("Хотите продолжить работать с тестом(1) или же хотите завершить работу(2)?");
            System.out.println("Введите цифру нужного Вам ответа");
            int answer = Integer.parseInt(scanner.nextLine());
            if (answer == 2) {
                break;
            }
        }
    }

    private void creatorStep() {
        addQuestions();
        do {
            System.out.println("На какое количество вопросов будет отвечать пользователь?");
            int questionsCountAvailabelForUser = Integer.parseInt(scanner.nextLine());
            int questionsCount = questions.size();
            if (questionsCountAvailabelForUser > questionsCount) {
                System.out.println("Извините, в базе недостаточно вопросов. Пожалуйста, добавьте недостающее число вопросов(1) или же укажите другое количество вопросов(2)");
                switch (Integer.parseInt(scanner.nextLine())) {
                    case 1:
                        addQuestions();
                        break;
                    case 2:
                        continue;
                }
            }
            if (questionsCountAvailabelForUser < questionsCount) {
                Collections.shuffle(questions);
                availableQuestionsForUser = copyElementsFromArrayList(questionsCountAvailabelForUser);
            }
            if (questionsCountAvailabelForUser == questionsCount) {
                availableQuestionsForUser = questions;
            }
            break;
        }
        while (true);
    }

    private void userStep() {
        System.out.println("Что же, давайте начинать. Вам будут предложены вопросы и варианты ответов. Чтобы выбрать вариант ответа, который Вы считаете верным, введите номер этого ответа.\nПо окончании теста, Вы узнаете, сколько введенных ответов были правильными. Удачи!");
        getQuestionsFromUser(availableQuestionsForUser);
        endTime = System.currentTimeMillis();
        double execTime = (endTime - startTime) * 1.0 / 1000;
        System.out.print("Тест подошел к концу. Ваш итоговый балл - " + totalScore + ". Хорошая работа! Ваше время выполнения теста - ");
        System.out.printf("%.2f\n", execTime);
        if (wrongQuestions.size() != 0) {
            System.out.println("Вопросы, на которые Вы ответили неправильно:");
            int questionNum = 0;
            for (Question question : wrongQuestions) {
                System.out.println(++questionNum + ". " + question + "?");
                System.out.println("Ваш ответ: " + question.getUserAnswers());
                for (Answer answer : question.getAnswers()) {
                    if (answer.getStatus().equals(Status.RIGHT)) {
                        System.out.println("Правильный ответ: " + answer);
                    }
                }
            }
        }
    }

    private void getQuestionsFromUser(ArrayList<Question> availableQuestionsForUser) {
        int answerNumber;
        Answer userAnswer;
        wrongQuestions = new ArrayList<>();
        boolean hasWrongAnswer;
        int questionNumber;
        ArrayList<Answer> userAnswers = new ArrayList<>();
        for (Question question : availableQuestionsForUser) {
            userAnswers.clear();
            questionNumber = 0;
            answerNumber = 0;
            hasWrongAnswer = false;
            System.out.println(++questionNumber + ". " + question + "?");
            System.out.println("Варианты ответов:");
            for (Answer answer : question.getAnswers()) {
                System.out.println(++answerNumber + ". " + answer);
            }
            System.out.println(question.getCountOfRightAnswers() > 1 ? "В данном вопросе несколько правильных вариантов ответа.(" + question.getCountOfRightAnswers() + ") Введите их через пробел" : "Введите номер правильно ответа");
            String[] strAnswers = scanner.nextLine().split(" ");
            for (String str : strAnswers) {
                userAnswer = question.getAnswers().get(Integer.parseInt(str) - 1);
                userAnswers.add(userAnswer);
            }
            for (Answer answer : userAnswers) {
                if (answer.getStatus().equals(Status.RIGHT)) {
                    totalScore += answer.getPrice();
                } else {
                    hasWrongAnswer = true;
                }
            }
            if (hasWrongAnswer) {
                wrongQuestions.add(question);
                question.setUserAnswers(userAnswers);
            }
        }
    }

    public Quiz() {
        questions = new ArrayList<>();
    }

    private ArrayList<Question> copyElementsFromArrayList(int countOfElementsToCopy) {
        ArrayList<Question> result = new ArrayList<>();
        for (int i = 0; i < countOfElementsToCopy; i++) {
            result.add(questions.get(i));
        }
        return result;
    }
}
