import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Question {
    private String question;
    private ArrayList<Answer> answers;
    private ArrayList<Answer> userAnswers = new ArrayList<>();
    boolean isMulti;
    private int countOfRightAnswers;

    public Question(String question, ArrayList<Answer> answers, boolean isMulti, int countOfRightAnswers) {
        this.question = question;
        this.answers = answers;
        this.isMulti = isMulti;
        this.countOfRightAnswers = countOfRightAnswers;
    }

    public int getCountOfRightAnswers() {
        return countOfRightAnswers;
    }

    public void setCountOfRightAnswers(int countOfRightAnswers) {
        this.countOfRightAnswers = countOfRightAnswers;
    }

    public ArrayList<Answer> getUserAnswers() {
        return userAnswers;
    }

    public void setUserAnswers(ArrayList<Answer> userAnswers) {
        this.userAnswers.addAll(userAnswers);
    }


    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    @Override
    public String toString() {
        return question;
    }
}
