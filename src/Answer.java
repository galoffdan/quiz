public class Answer {
    private String answer;
    private Status status;
    private double price;

    public Answer(String answer, Status status, double price) {
        this.answer = answer;
        this.status = status;
        this.price = price;
    }

    @Override
    public String toString() {
        return answer;
    }

    public Status getStatus() {
        return status;
    }

    public double getPrice() {
        return price;
    }

}
